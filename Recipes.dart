import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

import 'package:url_launcher/url_launcher.dart';

class Recipes extends StatefulWidget {
  @override
  _RecipesState createState() => _RecipesState();
}

class _RecipesState extends State<Recipes> {
  List<Map<String, String>> recipes = [
    {
      "name": "Mac & Cheese",
      "url":
          "https://www.momontimeout.com/best-homemade-baked-mac-and-cheese-recipe/",
      "image":
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQYG-0A4cJfDu2QK7dxol8G1r7cmAnWH0DJQ&usqp=CAU",
      "subtitle": "Serves 4\nPrep time: 30 min"
    },
    {
      "name": "Brocoli Salad",
      "url": "https://www.delish.com/cooking/nutrition/a28186450/keto-broccoli-salad-recipe/",
      "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDayabPlK3_DMwTBjrl9Tugo_hphQn_5OXJg&usqp=CAU",
      "subtitle": "Serves 8\nPrep time: 16 min"
    },
    {
      "name": "Veggie Pizza",
      "url": "https://www.culinaryhill.com/vegetable-pizza/",
      "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXSH5sF5LdtmDfNxiht4k2WWrOr7Ykr5ewkQ&usqp=CAU",
      "subtitle": "Serves 16\nPrep time: 1 hr 10 min"
    },
  ];

  Widget _buildList() {
    return ListView.builder(
        itemCount: recipes.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          final doc = recipes[index];
          return Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Container(
              padding: const EdgeInsets.fromLTRB(4, 4, 4, 4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xff222444),
              ),
              child: ListTile(
                isThreeLine: true,
                subtitle: Text(
                  doc["subtitle"],
                  style: TextStyle(
                    color: Color(0xfffbfbfb),
                    fontSize: 16,
                  ),
                ),
                leading: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(doc["image"]),
                ),
                title: RichText(
                  text: TextSpan(
                    text: doc["name"],
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        /// IMPORTANT: The following will give an error because
                        /// url_launched is no longer supported and a
                        /// replacement package has not yet been released.
                        if (await canLaunch(doc["url"])) {
                          await launch(doc["url"]);
                        } else {
                          throw 'Could not launch' + doc["url"];
                        }
                      },
                    style: TextStyle(
                      color: Color(0xfffbfbfb),
                      fontSize: 18,
                      fontFamily: "Open Sans Hebrew",
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

  Widget _createPage(BuildContext context) {
    return Container(
      color: Color(0xff0e0f1f),
      height: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.only(
        top: 44,
      ),
      child: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Recipes",
                  style: TextStyle(
                    color: Color(0xfffff2f2),
                    fontSize: 48,
                    fontFamily: "Open Sans Hebrew",
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
            _buildList(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("Printing recipes");
    return Scaffold(
      appBar: AppBar(
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
