import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'FirebaseVar.dart';
import 'FoodInSpace.dart';
import 'CreateFoodItem.dart';
import 'Recipes.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

// TODO: Change rules for Cloud Firestore and Firestore Storage

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> firebaseApp = Firebase.initializeApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'No Food Waste',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: FutureBuilder(
            future: firebaseApp,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                print("You have an error! ${snapshot.error.toString()}");
                return Text('Oops! Something went wrong. Please try again');
              } else if (snapshot.hasData) {
                auth = FirebaseAuth.instance;
                firestoreInstance = FirebaseFirestore.instance;
                storageInstance = FirebaseStorage.instance;
                alreadySignedIn = auth.currentUser != null;
                return Scaffold(
                  body: alreadySignedIn ? MyHomePage() : TitlePage(),
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}

/// Signout or delete account for current users
class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool deleteAccount = false;

  Future<void> signout(BuildContext context) async {
    await auth.signOut();
    if (auth.currentUser == null) {
      print("Returning to starting page");
      while (Navigator.canPop(context)) {
        // Navigator.canPop return true until it reaches the first page
        Navigator.pop(context);
      }
      if (alreadySignedIn) {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TitlePage()),
        );
      }
    }
  }

  Future<void> alert(String alert, String message, {bool error = false}) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(alert),
            content: Text(message),
            actions: <Widget>[
              if (error)
                TextButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              if (!error)
                TextButton(
                  child: Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              if (!error)
                TextButton(
                  child: Text('Proceed'),
                  onPressed: () {
                    deleteAccount = true;
                    Navigator.of(context).pop();
                  },
                ),
            ],
          );
        });
  }

  Future<void> deleteUser(BuildContext context) async {
    await alert('Delete Account',
        'Are you sure you want to delete this account? This action is not reversible.');
    if (!deleteAccount) {
      return;
    }
    try {
      await auth.currentUser.delete();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'requires-recent-login') {
        await alert('Error',
            'Please re-authenticate, by signing out and then signing back in, before deleting the account.',
            error: true);
        while (Navigator.canPop(context)) {
          // Navigator.canPop return true until it reaches the first page
          Navigator.pop(context);
        }
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SetUser()),
        );
        return;
      }
    }
    if (auth.currentUser == null) {
      while (Navigator.canPop(context)) {
        // Navigator.canPop return true until it reaches the first page
        Navigator.pop(context);
      }
      if (alreadySignedIn) {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TitlePage()),
        );
      }
    }
  }

  Widget _createPage(BuildContext context) {
    return Container(
      color: Color(0xff0e0f1f),
      padding: const EdgeInsets.only(
        top: 44,
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
              child: Text(
                "Settings",
                style: TextStyle(
                  color: Color(0xfffff2f2),
                  fontSize: 48,
                  fontFamily: "Open Sans Hebrew",
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(4),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xfff5daba),
                    ),
                    child: TextButton(
                      child: Text(
                        "Log Out",
                        style: TextStyle(
                          color: Color(0xffc06f0c),
                          fontSize: 20,
                        ),
                      ),
                      onPressed: () {
                        signout(context);
                      },
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.fromLTRB(8, 8, 8, 16),
                  padding: EdgeInsets.all(4),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xfff5daba),
                  ),
                  child: TextButton(
                    child: Text(
                      "Delete Account",
                      style: TextStyle(
                        color: Colors.redAccent,
                        fontSize: 20,
                      ),
                    ),
                    onPressed: () {
                      deleteUser(context);
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("Settings page ready");
    return Scaffold(
      appBar: AppBar(
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

/// Login or sign up new users
class SetUser extends StatefulWidget {
  SetUser({Key key, this.signup = false}) : super(key: key);

  final bool signup;

  @override
  _SetUserState createState() => _SetUserState();
}

class _SetUserState extends State<SetUser> {
  String username;
  String password;
  final _formKey = GlobalKey<FormState>();

  void _signup(BuildContext context) async {
    try {
      await auth.createUserWithEmailAndPassword(
          email: username + "@example.com", password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('The password provided is too weak.')));
      } else if (e.code == 'email-already-in-use') {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text('Username is taken')));
      }
    } catch (e) {
      print(e);
    }
    if (auth.currentUser != null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MyHomePage()),
      );
    }
  }

  void _login(BuildContext context) async {
    try {
      await auth.signInWithEmailAndPassword(
          email: username + "@example.com", password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found' || e.code == 'wrong-password') {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Incorrect username and/or password.')));
      }
    }
    if (auth.currentUser != null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MyHomePage()),
      );
    }
  }

  Widget _createPage(BuildContext context) {
    return Container(
      color: Color(0xff0e0f1f),
      padding: const EdgeInsets.only(
        top: 44,
      ),
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                widget.signup ? "Sign Up" : "Log In",
                style: TextStyle(
                  color: Color(0xfffff2f2),
                  fontSize: 48,
                  fontFamily: "Open Sans Hebrew",
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0xff232544),
            ),
            child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Text(
                      widget.signup ? "Sign Up" : "Log In",
                      style: TextStyle(
                        color: Color(0xfffbfbfb),
                        fontSize: 26,
                        fontFamily: "Open Sans Hebrew",
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Color(0xffc4c4c4)),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter username';
                          }
                          username = value;
                          return null;
                        },
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontFamily: "Open Sans Hebrew",
                          fontWeight: FontWeight.w700,
                        ),
                        decoration: InputDecoration(
                          hintText: "Enter username",
                          errorStyle: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Color(0xffc4c4c4)),
                      child: TextFormField(
                        obscureText: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter password';
                          }
                          password = value;
                          return null;
                        },
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontFamily: "Open Sans Hebrew",
                          fontWeight: FontWeight.w700,
                        ),
                        decoration: InputDecoration(
                          hintText: "Enter password",
                          errorStyle: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 48, 0, 0),
            child: GestureDetector(
              onTap: () {
                if (_formKey.currentState.validate()) {
                  if (widget.signup) {
                    _signup(context);
                  } else {
                    _login(context);
                  }
                }
              },
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Color(0x8ec06f0c),
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: Color(0xd1c06f0c),
                  child: IconButton(
                      icon: widget.signup
                          ? Icon(Icons.person_add_rounded)
                          : Icon(Icons.person_outline_rounded),
                      iconSize: 32,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          if (widget.signup) {
                            _signup(context);
                          } else {
                            _login(context);
                          }
                        }
                      }),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("ready for log in");
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class TitlePage extends StatefulWidget {
  @override
  _TitlePageState createState() => _TitlePageState();
}

class _TitlePageState extends State<TitlePage> {
  Widget _createPage(BuildContext context) {
    return Container(
      color: Color(0xff0e0f1f),
      padding: const EdgeInsets.only(
        top: 64,
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: CircleAvatar(
              radius: 150,
              backgroundColor: Colors.deepOrangeAccent,
              child: ListTile(
                  //leading: TODO Set this equal to a the image of an orange,
                  title: Center(
                child: Text(
                  "EAT.ME",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 48,
                    fontFamily: "Amatic SC",
                    fontWeight: FontWeight.w700,
                  ),
                ),
              )),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(4),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xfff5daba),
                    ),
                    child: TextButton(
                      child: Text(
                        "Log In",
                        style: TextStyle(
                          color: Color(0xffc06f0c),
                          fontSize: 36,
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SetUser(signup: false),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.fromLTRB(8, 8, 8, 16),
                  padding: EdgeInsets.all(4),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xfff5daba),
                  ),
                  child: TextButton(
                    child: Text(
                      "Sign Up",
                      style: TextStyle(
                        color: Color(0xffc06f0c),
                        fontSize: 36,
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SetUser(signup: true),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("title set");
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

/// Displays the main page with all the spaces
class SpacesListItem extends StatelessWidget {
  const SpacesListItem({Key key, this.title, this.header = false})
      : super(key: key);

  final String title;
  final bool header;

  @override
  Widget build(BuildContext context) {
    if (!header) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Color(0xff232544),
          ),
          child: ListTile(
              title: Text(
                title,
                style: TextStyle(
                  color: Color(0xfffbfbfb),
                  fontSize: 26,
                  fontFamily: "Open Sans Hebrew",
                  fontWeight: FontWeight.w700,
                ),
              ),
              trailing: Icon(Icons.arrow_forward_ios_rounded,
                  size: 26, color: Colors.white),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FoodInSpace(title: title),
                  ),
                );
              }),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: Align(
        alignment: Alignment.topLeft,
        child: Text(
          title,
          style: TextStyle(
            color: Color(0xfffff2f2),
            fontSize: 48,
            fontFamily: "Open Sans Hebrew",
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<bool> _isSelected;
  OverlayEntry overlayEntry;

  @override
  void initState() {
    _isSelected = [true, false, false];
    super.initState();
  }

  Widget _toggleButons(BuildContext context) {
    return Align(
        alignment: Alignment.bottomCenter,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Container(
                width: MediaQuery.of(context).size.width - 32,
                padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(30.0)),
                  color: Color(0x11ccced1),
                ),
                child: Flex(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: GestureDetector(
                          onTap: () {Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => CreateFoodItem(),
                            ),
                          );},
                          child: Column(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 50,
                                backgroundColor: Color(0x8ec06f0c),
                                child: CircleAvatar(
                                  radius: 30,
                                  backgroundColor: Color(0xd1c06f0c),
                                  child: IconButton(
                                      icon: Icon(Icons.camera_alt_rounded),
                                      iconSize: 32,
                                      onPressed: () {Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => CreateFoodItem(),
                                        ),
                                      );}),
                                ),
                              ),
                              Text(
                                "Add Item",
                                style: TextStyle(
                                  color: Color(0xfff5daba),
                                  fontSize: 14,
                                  fontFamily: "Open Sans Hebrew",
                                  fontWeight: FontWeight.w700,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: <Widget>[
                                Icon(Icons.food_bank, size: 32),
                                Text(
                                  "Spaces",
                                  style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 14,
                                    fontFamily: "Open Sans Hebrew",
                                    fontWeight: FontWeight.w700,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Recipes(),
                                ),
                              );},
                              child: Column(
                              children: <Widget>[
                                Icon(Icons.fastfood, size: 32),
                                Text(
                                  "Recipes",
                                  style: TextStyle(
                                    color: Color(0xfff5daba),
                                    fontSize: 14,
                                    fontFamily: "Open Sans Hebrew",
                                    fontWeight: FontWeight.w700,
                                  ),
                                )
                              ],
                            ),
                          ),
                          ),
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SettingsPage(),
                                ),
                              );},
                              child: Column(
                              children: <Widget>[
                                Icon(Icons.settings, size: 32),
                                Text(
                                  "Settings",
                                  style: TextStyle(
                                    color: Color(0xfff5daba),
                                    fontSize: 14,
                                    fontFamily: "Open Sans Hebrew",
                                    fontWeight: FontWeight.w700,
                                  ),
                                )
                              ],
                            ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ))));
  }

  Widget _createPage(BuildContext context) {
    return Container(
      color: Color(0xff0e0f1f),
      padding: const EdgeInsets.only(
        top: 44,
      ),
      child: Column(
        children: [
          SpacesListItem(title: "Spaces", header: true),
          SpacesListItem(title: "Fridge"),
          SpacesListItem(title: "Pantry"),
          SpacesListItem(title: "Snack Drawer"),
          Spacer(),
          _toggleButons(context),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("initialized");
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
