import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'FirebaseVar.dart';

class FoodListItem extends StatelessWidget {
  const FoodListItem(
      {Key key, this.space, this.thumbnail, this.title, this.expiration, this.id})
      : super(key: key);

  final String space;
  final Image thumbnail;
  final String title;
  final int expiration;
  final String id;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xff222444),
        ),
        child: ListTile(
          subtitle: Text(
            "Expires in " + expiration.toString() + " days",
            style: TextStyle(
              color: Color(0xfffbfbfb),
              fontSize: 16,
            ),
          ),
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: thumbnail,
          ),
          title: Text(
            title,
            style: TextStyle(
              color: Color(0xfffbfbfb),
              fontSize: 18,
              fontFamily: "Open Sans Hebrew",
              fontWeight: FontWeight.w700,
            ),
          ),
          trailing: IconButton(
              icon: Icon(Icons.cancel, size: 26, color: Colors.red),
              onPressed: () {
                firestoreInstance.collection(space).doc(id).delete();
              }),
        ),
      ),
    );
  }
}

class FoodInSpace extends StatefulWidget {
  FoodInSpace({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _FoodInSpaceState createState() => _FoodInSpaceState();
}

class FoodItem {
  const FoodItem(
      {this.thumbnail, this.title, this.expiration});

  final Image thumbnail;
  final String title;
  final int expiration;
}

class _FoodInSpaceState extends State<FoodInSpace> {

  Widget _buildList(QuerySnapshot snapshot) {
    print("Items in this space: ");
    print(snapshot.docs.length);
    for(int i = 0; i < snapshot.docs.length; ++i) {
      print(snapshot.docs[i].id);
      print(snapshot.docs[i]["name"]);
      print(snapshot.docs[i]["expirationDate"]);
    }

    return ListView.builder(
        itemCount: snapshot.docs.length,
        itemBuilder: (context, index) {
          final doc = snapshot.docs[index];
          return Dismissible(
            key: Key(doc.id),
            background: Container(color: Colors.red),
            onDismissed: (direction) {
              // delete the doc from the database
              firestoreInstance.collection(widget.title).doc(doc.id).delete();
            },
            child: FoodListItem(
              title: doc["name"],
              expiration: (doc["expirationDate"] as Timestamp).toDate().difference(DateTime.now()).inDays,
              thumbnail: Image.network(doc["image"]),
              id: doc.id,
              space: widget.title
            ),
          );
        });
  }

  Widget _createPage(BuildContext context) {
    Stream<QuerySnapshot> stream = firestoreInstance
        .collection(widget.title)
        .orderBy('expirationDate', descending: false)
        .snapshots();

    return Container(
      color: Color(0xff0e0f1f),
      padding: const EdgeInsets.only(
        top: 44,
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                widget.title,
                style: TextStyle(
                  color: Color(0xfffff2f2),
                  fontSize: 48,
                  fontFamily: "Open Sans Hebrew",
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          StreamBuilder<QuerySnapshot>(
              stream: stream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return LinearProgressIndicator();
                } else {
                  return Expanded(child: _buildList(snapshot.data));
                }
              }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
