## EAT.ME

Can you list all the food items currently in your kitchen? Probably not. And that's a problem because it leads to food waste.

How? Well, if you don't know what food items you have, how can you possibly eat them. So, those items just sit their in your kitchen, forgotten, doomed to go to waste. They only come to our attention when their rotten stench pervades the house. But, by then its too late. They must be tossed into the trash can.

EAT.ME designed to tackle this problem, to remind us of the food our kitchen houses, to save food from the trash can.

**How EAT.ME works**

EAT.ME is a mobile application (for android and iOS) which stores all the food items a user has in their kitchen.

After creating an account, the user can store a new food item by providing the food item's name, expiration date, and food item's photo. To further help the user keep track of the food item, they are also encouraged to list which "space" they stored the food item in. Currently, the app supports 3 spaces, fridge, pantry, and snack drawer.

The user can tap on any of the spaces listed in the main page of the app and it will show them all the food items they have in that space, along with the food items' photos and numbers of days till expiry.

For example, say a user buys pasta and tomatoes. They will create two new food items for each of the items, providing the items name, expiry date, photo, and then placing the pasta in "pantry" space and tomatoes in "fridge" space.

EAT.ME also has a recipes page which displays various recipes a user can try out. Clicking on a recipe takes the user to a webpage containing the recipe instructions.

**Installation steps**

1. Clone all the files from this repo, except .txt files.
2. Create a Flutter project in either Android Studio or XCode.
3. In the Flutter project, copy contents from our pubspec.yalm file into your pubspec.yalm file.
4. Copy all the .dart files in /lib/ folder in your flutter project.
6. Link Flutter project with Firebase.
5. Get all the dependencies by running pub get.
6. Your are set. Run on an emulator or a real phone.