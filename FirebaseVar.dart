import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseAuth auth;
FirebaseFirestore firestoreInstance;
FirebaseStorage storageInstance;
bool alreadySignedIn;