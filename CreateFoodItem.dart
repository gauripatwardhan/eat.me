import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';

import 'FirebaseVar.dart';

class CreateFoodItem extends StatefulWidget {
  @override
  _CreateFoodItemState createState() => _CreateFoodItemState();
}

class _CreateFoodItemState extends State<CreateFoodItem> {
  String imageFile;
  PickedFile pickedFile;
  String foodName;
  String space = "Fridge";
  DateTime selectedDate =
  DateTime.now(); // The number of days the food item can live
  final _formKey = GlobalKey<FormState>();

  Future uploadFile() async {
    print("Uploading file");
    try {
      await storageInstance
          .ref(foodName + '.png')
          .putFile(File(pickedFile.path));
    } catch (e) {
      // e.g, e.code == 'canceled'
    }
    imageFile = await storageInstance.ref(foodName + '.png').getDownloadURL();
    print("File uploaded");
  }

  Future<void> _openGallery(BuildContext context) async {
    pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
  }

  void _createFoodItem(BuildContext context) async {
    await _openGallery(context);
    if (pickedFile != null) {
      await uploadFile();
      firestoreInstance.collection(space).add({
        "name": foodName,
        "expirationDate": selectedDate,
        "image": imageFile,
        "userId": auth.currentUser.uid
      });
      Navigator.pop(context);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Error: No image selected"),
      ));
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        selectedDate = picked;
      });
  }

  Widget _createPage(BuildContext context) {
    return Container(
      color: Color(0xff0e0f1f),
      padding: const EdgeInsets.only(
        top: 44,
      ),
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Add Item",
                style: TextStyle(
                  color: Color(0xfffff2f2),
                  fontSize: 48,
                  fontFamily: "Open Sans Hebrew",
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0xff232544),
            ),
            child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Text(
                      "Add New Item",
                      style: TextStyle(
                        color: Color(0xfffbfbfb),
                        fontSize: 26,
                        fontFamily: "Open Sans Hebrew",
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Color(0xffc4c4c4)),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter food item name first';
                          }
                          foodName = value;
                          return null;
                        },
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontFamily: "Open Sans Hebrew",
                          fontWeight: FontWeight.w700,
                        ),
                        decoration: InputDecoration(
                          hintText: "Enter food item name",
                          errorStyle: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Color(0xffc4c4c4)),
                      margin: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: GestureDetector(
                        onTap: () => _selectDate(context),
                        child: AbsorbPointer(
                          child: TextFormField(
                            validator: (value) {
                              if (selectedDate == null) {
                                return 'Please enter food item expiration date first';
                              }
                              if (selectedDate.isBefore(DateTime.now())) {
                                return 'Expiration date cannot be in the past';
                              }
                              return null;
                            },
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontFamily: "Open Sans Hebrew",
                              fontWeight: FontWeight.w700,
                            ),
                            decoration: InputDecoration(
                              hintText: selectedDate == null
                                  ? "Enter expiration date"
                                  : "${selectedDate.toLocal()}".split(' ')[0],
                              errorStyle: TextStyle(
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Color(0xffc4c4c4)),
                      margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0), child: DropdownButton<String>(
                      value: space,
                      style: const TextStyle(color: Colors.black, fontSize: 18,
                          fontFamily: "Open Sans Hebrew",
                          fontWeight: FontWeight.w700),
                      underline: Container(
                        height: 2,
                        color: Colors.transparent,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          space = newValue;
                        });
                      },
                      items: <String>["Fridge", "Pantry", "Snack Drawer"]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),),
                  ],
                )),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 48, 0, 0),
            child: GestureDetector(
              onTap: () {
                if (_formKey.currentState.validate()) {
                  // TODO: How to access camera? Probably can't access camera on emulator so may stick to using photos from library
                  _createFoodItem(
                      context); // Accesses photo library instead of camera
                }
              },
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Color(0x8ec06f0c),
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: Color(0xd1c06f0c),
                  child: IconButton(
                      icon: Icon(Icons.camera_alt_rounded),
                      iconSize: 32,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          // TODO: How to access camera? Probably can't access camera on emulator so may stick to using photos from library
                          _createFoodItem(
                              context); // Accesses photo library instead of camera
                        }
                      }),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Eat.me"),
      ),
      body: _createPage(context),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}